(function () {
    var psConfig = PriceSpider.widgetConfigs["5c659d1212547c0024e4feff"];
    
    //--- VCI - start
    if (!('vciLoader' in window)) {
        window['vciLoader'] = true;
        var vciscript = document.createElement('script');
        vciscript.src = '//cdn.pricespider.com/1/lib/psdmwtbadtrack.js';
        document.head.appendChild(vciscript);
        window['vciLoader'] = window.setInterval(function(){if('psdm_wtb_loadTracker' in window){ window.clearInterval(window['vciLoader']); psdm_wtb_loadTracker({'part_key': '8ee7ccde-6ddb-4f04-901c-21fc076962d3' });}}, 200);
    }
    //--- VCI - end

    psConfig.on("data", function (widget) {
        if (widget && widget.data && (widget.data.onlineSellers && widget.data.onlineSellers.length || widget.data.localSellers && widget.data.localSellers.length)) {
            var lightLogos = {
                858947: widget.data.path + 'images/858947.png',
                858952: widget.data.path + 'images/858952.png',
                187: widget.data.path + 'images/187.png',
                2146: widget.data.path + 'images/2146.png',
                310: widget.data.path + 'images/310.png',
                2040543: widget.data.path + 'images/2040543.png',
                858953: widget.data.path + 'images/858953.png',
                841: widget.data.path + 'images/841.png',
                7352392: widget.data.path + 'images/7352392.png'
            }
            for (var i = 0; i < widget.data.onlineSellers.length; i++) {
                var seller = widget.data.onlineSellers[i];
                if (lightLogos.hasOwnProperty(seller.seller.id))
                    seller.seller.imageUrl = lightLogos[seller.seller.id];
            }
            for (var i = 0; i < widget.data.localSellers.length; i++) {
                var seller = widget.data.localSellers[i];
                if (lightLogos.hasOwnProperty(seller.seller.id))
                    seller.seller.imageUrl = lightLogos[seller.seller.id];
            }
        }
    });
})();