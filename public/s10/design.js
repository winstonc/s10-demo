(function ($) {
    $(function(){


        //
        //<!-- 3. Javascript -->

        // -- Internet Exploer scroll bug fix. -- //
        !function(){
            var ua = window.navigator.userAgent.toLowerCase();
            if( ua.indexOf('edge')>-1 || $.browser.ie ) {
                var eventWheel = function(e) {
                    var ieScrollTop = Math.floor(window.pageYOffset - e.wheelDelta);
                    window.scrollTo(0, ieScrollTop);
                }
                document.body.addEventListener('mousewheel.wheel', eventWheel);
            }
        }();


        var $window   = $(window),
            $document = $(document),
		isRtl     = $('html').hasClass('rtl'),
		isRtlPage = isRtl?-1:1,
		safari       = $('html').hasClass('safari');

        var InfinityDisplay = (function() {
            var $feature         = $('.m_feature-infinity'),
			$title           = $feature.find('.f_header-s10 .c_tit-type2'),
                $section2        = $feature.find('.f_section2'),
                $section3        = $feature.find('.f_section3'),
                $fContainer      = $feature.find('.f_container'),
                $fixedEndLine    = $feature.find('.fixed-endline'),
                $section2Header  = $feature.find('.f_header-type1'),
                $tabArea         = $feature.find('.o_tab'),
                $tabClickAble    = $tabArea.find('a'),
                $itemAreas       = $feature.find('.o_prod_areas'),
                $products        = $section2.find('.o_prod_areas>.o_prod'),
                $descs           = $feature.find('.o_item-desc'),
                $itemDescption   = $section2.find('.o_item-desc>.c_desc-type3'),
                $oItemArea       = $section3.find('.o_item-area'),
                $oItemArea2      = $section3.find('.g_s10 .o_prod'),

                $section3Product = null,
                pohoneTop        = 190,

                curType          = null,
                descCopy         = false,
                selected         = $tabArea.find("ul").data("btn-title"),
                a;

            var _init = function() {
                    if ( !$('.m_feature-infinity').length ) {
                        return false;
                    }

                    $tabClickAble.on('click.tab', tabClick);
                    curType = $tabClickAble.filter('.on').data('prod-type');
                    $tabClickAble.filter('.on').attr("title",selected);

                    hello();

                    if (GALAXY.sizeMode > 2) {
                        $('.o_prod').each(function(i,o) {
                            GALAXY.loadImage($(o), _scroll);
				 	});
                    }

                    return this;
                },
                hello = function() {
                    $feature.children().each(function(i, o) {
					var $o = $(o),
						$obj = $o.find('[class*="c_"], .f_container').not('.no-anim');

                        GALAXY.hello($o, {
                            on : function(){
                                var $objs = $o.find('[class*="c_"], .f_container').not('.no-anim');
                                $objs.each(function(i) {
                                    $(this).css({
                                        'transition-delay': (i*200)+'ms',
                                        '-webkit-transition-delay': (i*200)+'ms'
                                    });
                                });
                                $o.addClass('ani-fade');
                            },
                            off: function(){
                                var $objs = $o.find('[class*="c_"], .f_container').not('.no-anim');
                                $objs.css({
                                    'transition-delay': '',
                                    '-webkit-transition-delay': ''
                                });
                                $o.removeClass('ani-fade');
                            }
                        });
                    });
                },
                tabClick = function(e) {
                    var $this = $(this),
                        thisType = $this.data('prod-type'),
                        classProdName = $this.data('prod-names'),
                        thisIndex = $this.parent().index(),

                        lengs = $tabClickAble.length/$tabArea.length;

                    if ( !$this.hasClass('on') ) {
                        $feature.removeClass('s10e s10 s10plus').addClass(classProdName);
                        $tabClickAble.each(function(i,a) {
                            $(a).decideClass('on', ((i%lengs)==thisIndex) )
                            if ((i%lengs)==thisIndex) {
                                $(a).attr('title',selected)
                            } else {
                                $(a).attr('title','');
                            }
                        });
                        $itemDescption.removeClass('on').eq(thisIndex).addClass('on');
                        $products
                            .removeClass('on')
                            .eq(thisIndex).addClass('on');

                        if ( thisType != curType ) {
                            curType = thisType;
                            $oItemArea.find('>.o_item').removeClass('on');
                            $oItemArea.find('>.g_'+thisType).addClass('on');

                            var curIndex = Math.max(thisIndex-1,0);
                            curIndex = curIndex === 2 ? 1 : curIndex;

                            $oItemArea2.each(function(i, o) {
                                var $o = $(o);
                                $o.decideClass('on', !!(i==curIndex));
                            });
                            $(window).scroll();
                        } else {
                            $oItemArea2.each(function(i, o) {
                                var $o = $(o);
                                $o.decideClass('on', !$o.hasClass('on'));
                            });
                        }
                    }
                    e.preventDefault();
                },

                getPercent = function(p,maxValue) {
                    var p = p || 0,
                        max = maxValue||1;
                    return Math.min(Math.max(p,0),max);
                },

                _resize = function() {
                    var $cloneArea = $feature.find('.f_header-type1-clone'),
                        $oriArea = $feature.find('.f_header-type1 .o_area2'),
                        $gs10e = $section3.find('.g_s10e');
                    if (GALAXY.sizeMode<3) {
                        if ( $gs10e.length && $gs10e.hasClass('detach') ) {
                            $gs10e.removeClass('detach');
                            $gs10e.find('.o_tab').after($gs10e.find('.o_item-desc').detach());
                        }
                    } else {
                        if ( $gs10e.length && !$gs10e.hasClass('detach') ) {
                            $gs10e.addClass('detach');
                            $gs10e.find('.f_header-type1').append( $gs10e.find('.o_item-desc').detach() );
                        }
                    }

                    if (GALAXY.sizeMode<2) {
                        if ( !descCopy ) {
                            descCopy = true;
                            $oriArea.each(function(i) {
							var $desc = $(this).find('> .c_desc-type2').detach();
							var $logoWrap = $(this).find('.logo-wrap').detach();
							$cloneArea.eq(i).append($logoWrap);
                                $cloneArea.eq(i).append($desc);
                            });
                        }
                    } else {

                        if ( descCopy ) {
                            descCopy = false;
                            $cloneArea.each(function(i) {
							var $desc = $(this).find('> .c_desc-type2').detach();
							var $logoWrap = $(this).find('.logo-wrap').detach();
							$oriArea.eq(i).append($logoWrap);
                                $oriArea.eq(i).append($desc);
                            });
                        }
                    }
                },
                _scroll = function(scrollTop) {
                    var areaHeight = GALAXY.areaHeight,
                        clientTop = 0,
                        $elem;

                    // Line
                    $descs.each(function(i, elem) {
                        $elem = $(elem);
                        clientTop = this.getBoundingClientRect().top,
                            lineTop = $feature.hasClass('s10e')?300: 230;

                        if ( (areaHeight/7*6 >= clientTop) && (clientTop >= lineTop) ) {
                            if (!$elem.data('class')) {
                                $elem.data('class', true).addClass('view');
                            }
                        } else {
                            if ($elem.data('class')) {
                                $elem.data('class', false).removeClass('view');
                            }
                        }
                    });

                    if (GALAXY.sizeMode>2) {
                        var imageHeight  = $section2.find('.o_prod').height();
                        var isFixedStart = !!(GALAXY.scrollTop>=$section2.offset().top);
                        var isFixedEnd   = !!(GALAXY.scrollTop<=($fixedEndLine.offset().top-imageHeight-pohoneTop));
                        var deltaY = ($fixedEndLine.offset().top-imageHeight-pohoneTop) - $section2.offset().top;
					var productTop = 628 + ($title.outerHeight() - 120); //product style top value
					productTop = !(isFixedStart&&isFixedEnd)?productTop:'';

					    $itemAreas.__css({'top':productTop, y: (isFixedEnd?0:deltaY)});
                        $feature.decideClass('fixed', isFixedStart && isFixedEnd);
                        $feature.decideClass('step2', ($section3.offset().top-pohoneTop) <= GALAXY.scrollTop);
                    } else {
                        $feature.removeClass('step2');
                        $feature.removeClass('fixed');
					    $itemAreas.__css({top:'',y: 0});
                    }

                    return this;
                };

            return {
                initalize: _init,
                resize   : _resize,
                scroll   : _scroll
            }
        })().initalize();

        var EyeCare = (function() {

            var $feature = $('.m_feature_eye-care'),
                $fContainer  = $feature.find('.f_container'),
                $imgWrapper  = $feature.find('.o_content'),
                $figure      = $feature.find('figure.phone'),
                $mask        = $feature.find('figure.mask'),
                $point       = $feature.find('.o_point'),
                isDrag       = false,
                isOmni       = false,
                dragPoint    = {x:0, scrollX: 0},
                isScrollMode = false;

            var _init = function() {
                    if ( !$('.m_feature_eye-care').length ) {
                        return false;
                    }
                    GALAXY.load(load);
                    GALAXY.resize(function(){
                        isScrollMode = !!(GALAXY.sizeMode==1) && $mask.width()-60>GALAXY.areaWidth;
                        $fContainer.decideClass('scroll', isScrollMode);
					    $imgWrapper.attr('tabIndex', (isScrollMode)?0:'');
                    })
                    return this;
                },
                addEvent = function() {
                    $imgWrapper.on('mousedown.eyeCareDrag', drag.start).on('scroll.eyeCareDrag', scrollMove);
                    $document.on({'mousemove.eyeCareDrag': drag.move, 'mouseup.eyeCareDrag': drag.end});
                },
                removeEvent = function() {
                    $imgWrapper.off('.eyeCareDrag');
                    $document.off('.eyeCareDrag');
                },
                scrollMove = function() {
				var scrollLeft = (safari&&isRtl)?-$imgWrapper.scrollLeft():$imgWrapper.scrollLeft();
					size = ($figure.width() - $imgWrapper.width())/2,
					step = !!(scrollLeft>size),
                        $points = $point.find('span');

				step = (isRtl&&!safari)?!step:step;


                    $points.eq(0).decideClass('on', !step);
                    $points.eq(1).decideClass('on', step);
                },
                load = function() {
					_off();
                    if ( $feature.find('.svg>video').length ) {
                        $feature.find('.svg>video').on('ended', function() {
                            $mask._animate({opacity:0.42}, {queue: false, duration:750, easing: 'easeOutCubic'});
                        }).on('playing', function() {
                            $mask._css({opacity:1})
                        });
                    }
                },
                drag = {
                    start: function(e) {
                        if (isScrollMode) {
                            $imgWrapper.stop(true);
                            e.preventDefault();
                            isDrag = true;
                            dragPoint.scrollX = $imgWrapper.scrollLeft();
                            dragPoint.x = e.pageX;
                        }
                    },
                    move: function(e) {
                        if (isScrollMode) {
                            var moveX = dragPoint.x - e.pageX;
                            if (isDrag === true) {
                                $imgWrapper.scrollLeft(moveX + dragPoint.scrollX);
                            }
                        }
                    },
                    end : function(e) {
                        isDrag = false;
                    }
                },
                _on = function() {
                    if ( GALAXY.sizeMode == 1 ) {
					if ( safari && isRtl ) {
						scrollStart = $imgWrapper.width() * isRtlPage;
						scrollEnd   = 0;
					} else {
					var scrollStart = isRtl?0:$imgWrapper.width(),
						scrollEnd   = isRtl?$imgWrapper.width():0;
					}
					$imgWrapper.scrollLeft(scrollStart);
					$imgWrapper.__animate({'scrollLeft': scrollEnd}, {duration: 1450, delay: 450, easing: 'easeInOutExpo', complete:function() {
                                if (!isOmni) {
                                    isOmni = true;
                                    $imgWrapper.one('scroll.dragOmni', function() {
                                        GALAXY.omniture('galaxy-s10:design:dynamic-amoled:drag:1', 'microsite_contentinter');
                                    })
                                }
                            }});
                    }
                    if ( !$feature.find('.svg>video').length ) {
                        $mask._animate({opacity:0.5}, {queue: false, duration:750, delay: 1250, easing: 'easeOutCubic'});
                    }
                },
                _off = function() {
				var scrollLeft = isRtl&&!safari?0:$imgWrapper.width();
                    if ( GALAXY.sizeMode == 1 ) {
					$imgWrapper.scrollLeft( scrollLeft );
                    }
                    $mask._stop(true).__css({opacity:1});
                },
                _resize = function() {
                    if (GALAXY.sizeMode == 1) {
                        addEvent();
                    } else {
                        removeEvent();
                    }
                };
            return {

                on: _on,
                off: _off,
                resize: _resize,
                initalize: _init
            }
        })().initalize();

        var Colors = (function() {
            var $wrapper           = $('.m_content-design.style'),
                $fakeBox            = $('.m_content_fake'),
                $feature            = $('.m_feature_colors'),
                $titleArea          = $feature.find('.title-area'),
                $tabHeaderArea      = $feature.find('.o_areas'),
                $tabClickAble       = $feature.find('.o_tab a'),
                $tabColors          = $feature.find('.o_tab-color'),
                $tabColorArea       = $feature.find('.o_tab-color-area'),
                $tabColorsClickAble = $tabColors.find('a'),
                $tabContainer       = $feature.find('.o_tab-cont'),
                $tabContents        = $tabContainer.find('>.phones'),
                $scrollContainer    = $feature.find('.o_area-inner'),
                $fContainer         = $feature.find('.f_container'),
                $selectContent      = $tabContainer.find('ul.o_between'),
                currentIndex        = -1,
                LowHeight           = 900,
                imageLength         = 11,
                colorDataArr        = [],
                loadImageCount      = 0,
                tempArr             = [],
                arrAlt              = [],
                arrZoomAlt          = [],
			    $bodyClickable      = null,
			    $voiceAreas         = null,
                tempScrollTop       = 0,

                lowMode             = false,
                $listItem           = null;

			var colorSelect = $tabColorArea.data("btn-title") + ':';
            var deviceSelect = $feature.find(".o_tab").find("ul").data("btn-title");


            var isFocus = null;

            var _init = function() {
                    if ( !$('.m_feature_colors').length ) {
                        return false;
                    }
                    $tabColors.find('a:first').addClass('on');

                    var $figure = $selectContent.find('figure'),
                        clickAbleTitle = $figure.find('a').attr('title');

                    $figure.each(function(i, figure) {
                        arrAlt[i] = $(figure).data('alt');
                        arrZoomAlt[i] = $(figure).data('zoom-alt')
                    });

                    for (var i=0, Html=''; i<imageLength;i++) {
                        Html += '<li class="o_list-item item'+(i+1)+'"><a href="#a" ga-ca="flagship pdp" ga-ac="gallery" ga-la="galaxy-s10:design:image-zoom-in" data-omni="galaxy-s10:design:image-zoom-in"data-omni-type="microsite_gallery" title="'+clickAbleTitle+'"><img src="//image-us.samsung.com/us/smartphones/galaxy-s10/v2/common/images/blank.gif" alt="'+arrAlt[i]+'"><span class="icon_plus"></span></a></li>';
                    }
                    $selectContent.html(Html);

                    $listItem = $selectContent.find('.o_list-item>a');

                    omni = $tabColors.filter('.on').find('a.on').data('omni').replace('color', 'drag');

                    _addEvent();
                    changeItems();
                    _off();
                    return this;
                },
                scrollX = (function() {
                    var isDrag;
                    var _init = function() {
                            isDrag = false,
                                dragPoint = {scrollX:0, x:0};
                            return this;
                        },
                        _start = function(e) {
                            if (e.button > 1) {return false;}
                            e.preventDefault();

                            $scrollContainer.stop(true);
                            isDrag = true;
                            dragPoint.scrollX = $scrollContainer.scrollLeft();
                            dragPoint.x = e.pageX;
                        },
                        _move = function(e) {
                            var moveX = dragPoint.x - e.pageX;
                            if (isDrag === true) {
                                $scrollContainer.scrollLeft(moveX + dragPoint.scrollX);
                                e.preventDefault();
                            }
                        },
                        _end  = function(e) {
                            isDrag = false;
                            e.preventDefault();
                        };

                    return {
                        initalize: _init,
                        drag: {
                            start: _start,
                            move : _move,
                            end  : _end
                        }
                    };
                })().initalize(),
                _addEvent = function() {
                    $tabColorsClickAble.on('click', colorChange);
                    $tabClickAble.on('click', productChange).filter(':last').addClass('on');
                    $listItem.on({'click.itemClick': zoom.open, 'keyup.itemKeyup': itemFocus});

				GALAXY.load(_off);
                    GALAXY.resize(function() {
					var direction = $('html').hasClass('rtl')?'paddingLeft':'paddingRight';
                        if (GALAXY.sizeMode <= 2) {
						$selectContent.css({direction: (GALAXY.areaWidth-$tabHeaderArea.width())/2});
                        } else {
						$selectContent.css({direction: ''});

                        }
                    });
                },
                itemFocus = function(e) {
                    if (e.keyCode == 9 && GALAXY.sizeMode > 2) {
                        var curIndex = $(this).parent().index();
                        if ( curIndex == 10) {
                            GALAXY.setScrollTop(tempScrollTop + 1);
                        } else {
                            var screenLowDeltaHeight = lowMode ? $titleArea.outerHeight() : 0;
                            var totWidth = (function() {
                                var w = 0;
                                $listItem.each(function(i,o) {
                                    if (i<curIndex) {
                                        w += $(o).width();
                                    }
                                });
                                return w;
                            })();
                            var top = $wrapper.offset().top + screenLowDeltaHeight + totWidth;

                            $fContainer.scrollLeft(0);
                            $('html, body').scrollTop(top);

                        }
                    }
                },
                _on = function() {
				if (safari&isRtl) {
					var scrollLeftStrat = $selectContent.width()*isRtlPage,
						scrollLeftEnd = 0;
				} else {
					var scrollLeftStrat = isRtl?0:$selectContent.width(),
						scrollLeftEnd = isRtl?$selectContent.width():0;
				}

                    if (GALAXY.sizeMode <= 2) {
					$scrollContainer.scrollLeft(scrollLeftStrat);
					$scrollContainer.__animate({'scrollLeft': scrollLeftEnd}, {duration: 1450, easing: 'easeInOutExpo', complete: function() {
                                $scrollContainer.off('scroll.omni').one('scroll.omni', function() {
                                    GALAXY.omniture(omni, 'microsite_contentinter');
                                });
                            }});
                    }
                },
                _off = function() {
				var scrollLeft = isRtl&&!safari?0:$selectContent.width();
                    if (GALAXY.sizeMode <= 2) {
					$scrollContainer.scrollLeft( scrollLeft );
                    }
                },

                zoom = (function() {
                    var zoomInterval   = false,
                        $zoomPopup     = $feature.find('.f_zoom-popup'),
                        $zoomImgArea   = $zoomPopup.find('.o_zoom-img'),
                        $zoomCloseBtn  = $zoomPopup.find('.o_btn-close a'),
                        closeAnimation = false,
                        $clickAbleSave = null,
                        saveScrollTop;

                    var _open = function(e) {
                            if ( closeAnimation ) return;

                            $feature.addClass('loading');

                            $clickAbleSave = $(this);
                            var $curImage = $clickAbleSave.find('img'),
                                $newImage = $('<img>'),
                                zoomImageSrc = $curImage.attr('src').replace('.jpg', '_zoom.jpg'),
                                moveScrollX = GALAXY.scrollTop + $tabContainer.__css('x')*isRtlPage,
                                curIndex = $(this).parent().index();
                            saveScrollTop = GALAXY.scrollTop;

                            $feature.removeClass('view-pop remove-view-pop');
                            $zoomImgArea.find('img').remove();

                            $feature.addClass('view-pop');
                            if($curImage.attr('src').indexOf('.png') > -1)
                                zoomImageSrc = $curImage.attr('src').replace('.png', '_zoom.png');


                            var zoomAlt = arrZoomAlt[curIndex];

                            $newImage.attr({alt: zoomAlt, src: zoomImageSrc}).appendTo($zoomImgArea);

                            $zoomImgArea.__css({x:0, y: 0});
                            $zoomCloseBtn.css({opacity: 0});
                            GALAXY.loadImage($newImage, function() {
                                zoomDrag.on();
                                $zoomPopup.addClass('inview');
                                $selectContent.css({position: 'relative',width: '100%', visibility: 'hidden'});

                                $zoomCloseBtn.stop().animate({opacity:1}, 1000);
                                $feature.removeClass('loading');

                                GALAXY.setScrollTop(moveScrollX);
                                webAccessibility.on();

                                if(GALAXY.sizeMode == 1) {
                                    GALAXY.noScroll.on();
                                    // webAccessibility.on();

                                    zoomDrag.isDragStart = true;
                                    zoomDrag.dragEnd();
                                }
                            });
                            $zoomCloseBtn.one('click.galaxyPopupClose', _close);
                            $zoomCloseBtn.off('keyup.galaxyPopupKeyup').on('keyup.galaxyPopupKeyup', _closeKeyup);

                            $('.m_feature_colors.view-pop .f_header-type1 a').attr('tabindex', '-1');
                            $('.m_feature_colors.view-pop .f_container a').attr('tabindex', '-1');

                            if (e) e.preventDefault();
                        },
                        _close = function(e) {
                            closeAnimation = true;
					        webAccessibility.off();

                            GALAXY.setTransitionEndEvent($zoomPopup, function() {
                                closeAnimation = false;
                                $feature.removeClass('view-pop remove-view-pop');
                                // if (GALAXY.sizeMode == 1) {
                                GALAXY.noScroll.off();
                                // }
                            });
                            $zoomCloseBtn.css({opacity:0});
                            $selectContent.css({position: '',width: '', visibility: ''});
                            zoomDrag.off();
                            GALAXY.setScrollTop(saveScrollTop);
                            GALAXY.scroll();
                            $('.m_feature_colors.view-pop .f_header-type1 a').attr('tabindex', '');
                            $('.m_feature_colors.view-pop .f_container a').attr('tabindex', '');
                            $feature.addClass('remove-view-pop');
                            $clickAbleSave && $clickAbleSave.focus();


                            if (e) e.preventDefault();
                        },
                        webAccessibility = {
                            on: function() {
                                $bodyClickable = $('body').find('a,input,select,textarea,button,video,iframe,.o_scroll');
                                $bodyClickable.each(function(e, el) {
                                    var tabindex = $(el).attr('tabindex');
                                    if (tabindex!==undefined&&tabindex!==null) {
                                        $(el).data('prev-tabindex', tabindex);
                                    }
                                    $(el).attr('tabindex','-1');
                                });
                                $zoomImgArea.attr('tabindex', '0').focus();
                                $zoomCloseBtn.attr('tabindex', '0');
                                $voiceAreas = $('#subnav');

                                //voice over
                                if (!GALAXY.isGalaxy) {
                                    $voiceAreas = $voiceAreas
                                                    .add('#header, #footer')
                                                    .add('.s-gotop-wrap')
                                                    .add('.m_dotcom_footer');
                                } else {
                                    $voiceAreas = $voiceAreas
                                        .add('#wrap > *:not(#contents)');
                                }
                                $voiceAreas = $voiceAreas
                                        .add('.m_jump_controls_arrow, .m_jump_controls')
                                        .add('.new_ article:not(.m_feature_colors)')
                                        .add('.m_feature_colors > *:not(.f_zoom-popup)');

                                $voiceAreas.attr('aria-hidden', 'true');
                            },
                            off: function() {
                                $bodyClickable.each(function(e, el) {
                                    var tabindex = $(el).data('prev-tabindex');
                                    if (tabindex!==undefined&&tabindex!==null) {
                                        $(el).attr('tabindex',tabindex);
                                    } else {
                                        $(el).removeAttr('tabindex');
                                    }
                                });
                                $voiceAreas.each(function(e, el) {
                                    $(el).removeAttr('aria-hidden');
                                });
                            }
                        },
                        _closeKeyup = function() {
                            GALAXY.setScrollTop(tempScrollTop + 1);
                        },
                        zoomDrag = {
                            temp  : {x:0, y:0},
                            start : {x:0, y:0},
                            delta : {x:0, y:0},
                            offset: {x:0, y:0},
                            maxCanvas: {width:0, height:0},
                            $element: false,
                            isDragStart: false,
                            dragStart: function(e) {
                                e.preventDefault();
                                if (e.type == 'mousedown' && e.button > 1) {return false;}
                                zoomDrag.start.x = (e.type == 'touchstart')?e.originalEvent.touches[0].pageX:e.pageX;
                                zoomDrag.start.y = (e.type == 'touchstart')?e.originalEvent.touches[0].pageY:e.pageY;

                                zoomDrag.temp = zoomDrag.getOffset();
                                zoomDrag.isDragStart = true;
                            },
                            dragMove: function(e) {
                                if (zoomDrag.isDragStart) {
                                    var move = {
                                        x: (e.type == 'touchmove')?e.originalEvent.touches[0].pageX:e.pageX,
                                        y: (e.type == 'touchmove')?e.originalEvent.touches[0].pageY:e.pageY
                                    };
                                    zoomDrag.delta = {
                                        x: move.x - zoomDrag.start.x,
                                        y: move.y - zoomDrag.start.y
                                    };

                                    zoomDrag.offset.x = zoomDrag.temp.x + zoomDrag.delta.x;
                                    zoomDrag.offset.y = zoomDrag.temp.y + zoomDrag.delta.y;

                                    $zoomImgArea.__css({x: zoomDrag.offset.x, y: zoomDrag.offset.y});
                                }
                            },
                            dragEnd : function(e) {
                                if (zoomDrag.isDragStart) {
                                    var mt = parseFloat(zoomDrag.$element.css('marginTop'));
                                    var mb = parseFloat(zoomDrag.$element.css('marginBottom'));
                                    zoomDrag.maxCanvas.width = zoomDrag.$element.width() - $zoomPopup.width();
                                    zoomDrag.maxCanvas.height = zoomDrag.$element.height()+mt+mb-$zoomPopup.height();

                                    if (zoomDrag.maxCanvas.width <= 0) {
                                        zoomDrag.offset.x = 0;
                                    } else {
                                        zoomDrag.offset.x = Math.max(zoomDrag.offset.x, zoomDrag.maxCanvas.width/2);
                                        zoomDrag.offset.x = Math.min(zoomDrag.offset.x, -zoomDrag.maxCanvas.width/2);
                                    }

                                    zoomDrag.offset.y = zoomDrag.offset.y + (zoomDrag.delta.y*2);

                                    if (zoomDrag.offset.y < -zoomDrag.maxCanvas.height) {
                                        zoomDrag.offset.y = -zoomDrag.maxCanvas.height;
                                    }
                                    zoomDrag.offset.y = Math.min(zoomDrag.offset.y, 0);

                                    $zoomImgArea.__animate({x:zoomDrag.offset.x, y:zoomDrag.offset.y}, {duration: 500, easing: 'easeOutQuint'});
                                    zoomDrag.isDragStart = false;
                                }
                            },
                            getOffset: function() {
                                return {x: $zoomImgArea.__css('x'), y: $zoomImgArea.__css('y')}
                            },
                            keyDown: function(e) {
                                var keyCode = e.keyCode;
                                delta = 50;

                                zoomDrag.isDragStart = !!(keyCode >= 37 && keyCode<= 40);

                                if (zoomDrag.isDragStart) {
                                    zoomDrag.temp = zoomDrag.getOffset();
                                    zoomDrag.delta.x = 0;
                                    zoomDrag.delta.y = 0;

                                    switch (keyCode) {
                                        case 37: zoomDrag.offset.x = zoomDrag.temp.x + delta; break;
                                        case 38: zoomDrag.offset.y = zoomDrag.temp.y + delta; break;
                                        case 39: zoomDrag.offset.x = zoomDrag.temp.x - delta; break;
                                        case 40: zoomDrag.offset.y = zoomDrag.temp.y - delta; break;
                                    };
                                    $zoomImgArea.__css({x: zoomDrag.offset.x, y: zoomDrag.offset.y});
                                }
                            },
                            on : function() {
                                $zoomImgArea.attr('tabIndex', 0).focus();
                                zoomDrag.$element = $zoomImgArea.find('img');

                                $zoomImgArea
                                    .on('mousedown.zoomImage touchstart.zoomImage', zoomDrag.dragStart)
                                    .on('mousemove.zoomImage touchmove.zoomImage', zoomDrag.dragMove);
                                $document
                                    .on('mouseup.zoomImage touchend.zoomImage', zoomDrag.dragEnd)
                                    .on('keydown.zoomImage', zoomDrag.keyDown)
                                    .on('keyup.zoomImage', zoomDrag.dragEnd);

                            },
                            off: function() {
                                $zoomImgArea.off('.zoomImage');
                                $document.off('.zoomImage');
                                $zoomImgArea.attr('tabIndex', '');
                            }
                        }

                    return {
                        init: _init,
                        open : _open,
                        close: _close
                    }
                })(),
                _resize = function() {

                    var sHeight = GALAXY.sizeMode>2?$fakeBox.height():'';
                    $wrapper.css({height: sHeight});

                    if (GALAXY.sizeMode <= 2) {
                        $scrollContainer.off('scroll.omni');
                        $scrollContainer.scrollLeft( $selectContent.width() );

                        scrollX && $scrollContainer
                            .on('mousedown.colorsScrollXEvent', scrollX.drag.start);
                        scrollX && $document
                            .on('mousemove.colorsScrollXEvent', scrollX.drag.move)
                            .on('mouseup.colorsScrollXEvent', scrollX.drag.end);

                    } else {
                        scrollX && $scrollContainer.off('.colorsScrollXEvent');
                        scrollX && $document.off('.colorsScrollXEvent');

					_off();
                    }

                    if ($('.m_feature_colors.view-pop').length) {
                        if (GALAXY.sizeMode == 1 || GALAXY.prevSizeMode == 1) {
                            $fakeBox.removeAttr('style');
                            zoom.close();
                        }
                    }


                    return this;
                },
                _scroll = function(scrollTop) {
                    if ( GALAXY.sizeMode > 2 ) {
                        lowMode = (GALAXY.areaHeight<=LowHeight);

                        var titleHeight = $titleArea.outerHeight()+parseFloat($feature.css('paddingTop')),
                            screenLowDeltaHeight = lowMode ? titleHeight : 0,
                            sectionOffset = scrollTop - $wrapper.offset().top - screenLowDeltaHeight,
                            fixedHeight = 0,
                            scrollWidth = $selectContent.width() - $tabContents.width() + fixedHeight,
                            isFixed = (sectionOffset >= 0) && sectionOffset <= scrollWidth,
                            top = isFixed&&lowMode ? -titleHeight : 0,
                            phoneScrollLeft = 0;

                        tempScrollTop = ($wrapper.offset().top + screenLowDeltaHeight + scrollWidth);

                        $wrapper.css({height: $fakeBox.height() + scrollWidth});
                        $wrapper.decideClass('fixed', isFixed);
                        $wrapper.decideClass('sH', lowMode);
                        $fakeBox.__css({y: top});


                        $fakeBox.css({'marginTop': (sectionOffset>scrollWidth)?scrollWidth:0});
                        phoneScrollLeft = Math.min(Math.max(sectionOffset, 0), scrollWidth - fixedHeight);
                        $tabContainer.__css({x: -phoneScrollLeft * isRtlPage});
                        $selectContent.css({'paddingRight':''});
                    } else {
                        $wrapper.css({height:''}).removeClass('fixed');
                        $fakeBox.css({'marginTop': 0});
                        $tabContainer.__css({x: 0});
                        $fakeBox.removeAttr('style');
                    }
                    return this;
                },

                colorChange = function(e) {
                    e.preventDefault();
                    var $this = $(this);
                    if ($this.hasClass('on')) {return false}

                    $this.siblings().removeClass('on');
                    $this.addClass('on');

                    changeItems();

                }
            productChange = function(e) {
                e.preventDefault();
                var $this = $(this);
                if ($this.hasClass('on')) {return false}
                var curindex = $tabClickAble.index(this);
                $(this).attr("title",deviceSelect).parent().siblings().find("a").attr("title","");
                $tabClickAble.removeClass('on').eq(curindex).addClass('on');
                $tabColors.removeClass('on').eq(curindex).addClass('on');

                changeItems();

            },
                crateImages = function(o) {
                    loadImageCount = 0;
                    tempArr = [];

                    for(var i=0, $o, src, alt; i<imageLength; i++) {
                        if(o.src.indexOf('s105g') > -1) {
                            src = '//image-us.samsung.com/us/smartphones/galaxy-s10/0701/' + o.src + (i + 1) + '.png'
                        }
                        else {
                            src = '//image-us.samsung.com/us/smartphones/galaxy-s10/v2/design/images/colors/' + o.src + (i + 1) + '.jpg';
                        }
                        alt = arrAlt[i];
                        // alt = (i == imageLength-1)?o.alt:'';

                        $o = $('<img />', {src: src, alt: alt});
                        tempArr.push($o);
                        GALAXY.loadImage($o, function() {
                            loadImageCallBack(o.key);
                        });

                    }
                },
                loadImageCallBack = function(o) {
                    loadImageCount++;
                    if (loadImageCount == imageLength) {
                        colorDataArr[o.key] = tempArr;
                        action(tempArr);
                    }
                },
                action = function($images) {
                    $listItem.each(function(i) {
                        var $this = $(this),
                            $img = $this.find('img');

                        $img.addClass('out')
                            ._animate({opacity: 0}, {duration:450, complete: function() {$img.remove();}});

                        $images[i].appendTo($this).css({opacity:0})
                    });
                    $feature.removeClass('loading');
                    TweenMax.staggerFromTo($images, .3, {opacity: 0}, {opacity: 1}, .1);
                },

                changeItems = function() {

                    $feature.addClass('loading');

                    var $curTabColor = $tabColors.filter('.on'),
                        $curColrChip = $curTabColor.find('a.on'),
                        oColorData = $curColrChip.data();
                    omni = $tabColors.filter('.on').find('a.on').data('omni').replace('color', 'drag');

                    $tabColors.filter('.on').find('.c_tit-type4').html($curColrChip.find('span.blind').text());

                    var screenLowDeltaHeight = lowMode ? $titleArea.outerHeight()+parseFloat($feature.css('paddingTop')) : 0;

                    GALAXY.sizeMode>2 && $wrapper.hasClass('fixed') && GALAXY.setScrollTop($wrapper.offset().top + screenLowDeltaHeight + 1);

				    // $tabClickAble.attr("title",deviceSelect);
                    $tabClickAble.each(function(i,t) {
                        var $tabA = $(t);
                        $tabA.attr('title',$tabA.hasClass('on')?deviceSelect:'');
                    });

                    $curTabColor.find('a').each(function(i,o) {
                        o.title = o.title.replace(colorSelect,'');
                        if ($(o).hasClass('on')) {
                            o.title = colorSelect+o.title;
                        }
                    });

                    if ( colorDataArr[oColorData.key] && colorDataArr[oColorData.key].length === imageLength ) {
                        action(colorDataArr[oColorData.key]);
                    } else {
                        crateImages(oColorData);
                    }
                    if ( GALAXY.sizeMode <= 2) {
                        $scrollContainer.off('scroll.omni').one('scroll.omni', function() {
                            GALAXY.omniture(omni, 'microsite_contentinter');
                        });
                    }
                }
            return {
                on: _on,
                off: _off,
                resize: _resize,
                scroll: _scroll,
                initalize: _init
            }
        })().initalize();

        var Dimension = (function() {
            if ( !$('.m_feature_dimension').length ) {
                return false;
            }
            var $feature = $('.m_feature_dimension'),
                $screens   = $feature.find('.screen'),
                $phoneArea = $feature.find('.o_dimension_phones'),
                $countInch = $feature.find('.c_prod-size .size-data'),
                $phones10plus = $feature.find('.phone2, .phone3'),
                $oriArea   = $feature.find('.f_header-type1'),
                $cloneArea = $feature.find('.f_header-type1-clone'),

                num        = 0,
                descCopy   = false,
                interval   = null;

            $countInch.each(function() {
                var $this = $(this);
                $this.data('size', parseFloat($this.text())).text('0.0');
            });

            var _on = function() {
                    setTimeout(function() {
                        $phoneArea.addClass('slide');
                    }, 600);
                    GALAXY.setTransitionEndEvent($phones10plus , function() {
                        clearTimeout(interval);
                        interval = setTimeout(function() {
                            $phoneArea.addClass('screen-off');
                            $screens.each(function(i, o) {
                                $(o)._animate({opacity:0}, {queue: false, duration:650, easing: 'easeInCubic', complete: function() {
                                        num = $countInch.eq(i).data('size');
                                        $({count:0})._animate({count: num}, {queue: false, duration: 850, rounding: false, step:function(e) {
                                                $countInch.eq(i).text(e.count.toFixed(1));
                                            }});
                                    }});
                            });
                        }, 600);
                    });

                },
                _off = function() {
                    clearTimeout(interval);
                    $screens._stop(true).__css({opacity:1});
                    $countInch.text('0.0');
                    $phoneArea.removeClass('slide screen-off');
                },
                _resize = function() {
				_off();
                    if (GALAXY.sizeMode<=2 ) {
                        if ( !descCopy) {
                            descCopy = true;
                            $cloneArea.append($oriArea.find('.c_desc-type2').detach());
                        }
                    } else {
                        if ( descCopy ) {
                            descCopy = false;
                            $oriArea.append($cloneArea.find('.c_desc-type2').detach());
                        }
                    }
                };

            return {
                on: _on,
                off: _off,
                resize: _resize
            }
        })();

        var OneUi = (function() {
            var $slider = $('.m_feature_one-ui ul.screens'),
                $paging = null,
                slideLengs = $slider.find('li').length,
                isCreatePaging = false;

            var _init = function() {
                    if ( !$('.m_feature_one-ui ul.screens').length ) {
                        return false;
                    }

                    return this;
                },
                _resize = function() {
                    if (GALAXY.sizeMode == 1) {
                        xlider.setup();
                    } else {
                        xlider.destroy();
                    }
                },
                xlider = {
                    setup: function() {
                        $paging = $('.m_feature_one-ui .o_paging>ul');

                        $slider.xlider({
                            swipe: GALAXY.swipeAble,
                            paging: $paging,
                            endless: true,
                            screenReader: true,
                            onChange: function(nowPage, max){
                            },
                            onChangeEnd: function(){}
                        });
                        $('.o_paging').show();


                    },
                    destroy: function() {
                        $slider.xlider('remove');
                        $slider.find('ul').removeAttr('style');
                        $slider.find('li').removeAttr('style');
                        $('.o_paging').hide();
                    }
                }
            return {
                initalize: _init,
                resize   : _resize
            }
        })().initalize();

        var OneUiYoutube = (function() {
            if ( !$('.o_youtube').length ) return false;

            var $feature = $('.m_feature_one-ui'),
                $oYoutubeArea = $('.o_yt-layer'),
                $btnPlay = $feature.find('.c_btn-play'),
                $btnClose = $oYoutubeArea.find('a.close'),
                videoId  = $btnPlay.data('youtube-id'),
                videoTitle  = $btnPlay.data('youtube-title'),
                youtubeUrl = GALAXY.getYoutubePlayerLink(videoId, 1);

            $btnPlay.on('click', function(e) {
                $feature.addClass('playing');
                 $('<iframe frameborder="0" tabindex="0" allowfullscreen="1" allow="autoplay" title="'+ videoTitle +'" src="'+ youtubeUrl +'"></iframe>').appendTo($oYoutubeArea).focus();

                $btnClose.focus();

                $('.o_youtube').attr('aria-hidden', true);
                $btnPlay.attr('aria-hidden', true);


                return false;
            });

            $btnClose.on('click', function () {
			    $('.o_youtube').removeAttr('aria-hidden');
			    $btnPlay.removeAttr('aria-hidden');
                $feature.removeClass('playing');
                $btnPlay.focus();
			    $oYoutubeArea.find('iframe').remove();
                return false;
            });
        })();

        var Package = (function() {
            if ( !$('.m_feature_package').length ) return false;

            var $feature      = $('.m_feature_package'),
                $oArea        = $feature.find('.o_area-inner'),
                $oScroll      = $feature.find('.o_scroll'),
                $oScrollInner = $feature.find('.o_prod-box'),
                $items        = $feature.find('.item-list'),
                $points       = null,
                isScrollMode  = false,
                isDrag        = false,
                isOneTagging  = false,
                scrollMaxValue = 0,
                scrollMinValue = 0;


            if ($items.length < 3) return false;

            var _resize = function(sizeMode) {
                    if ( (sizeMode==1) && $oScrollInner[0].scrollWidth >= $oScroll.width() ) {
					_off();
                        if (!isScrollMode) {
                            isScrollMode = true;
                            scrollMaxValue = $oScrollInner[0].scrollWidth - $oScrollInner.width();
                            scrollMinvalue = 0;
                            addEvent();
                        }
                    } else {
                        removeEvent();
                        $oScrollInner.__css({x:0});
                        isScrollMode = false;
                    }
                },
                addEvent = function() {
                    $oArea.addClass('scroll');
                    $points = $feature.find('.o_point>span');
                    $oScroll
					.attr('tabindex', '0')
					.on('mousedown.packageBox touchstart.packageBox', drag.start).on('keydown.packageBox', keydown);
                    $document.on({'mousemove.packageBox touchmove.packageBox': drag.move, 'mouseup.packageBox touchend.packageBox': drag.end});
                },
                removeEvent = function() {
                    $oArea.removeClass('scroll');
				$oScroll.off('.packageBox').attr('tabindex','');
                    $document.off('.packageBox');

                },
                _on = function() {
				var scrollLeftStrat = isRtl?0:-scrollMaxValue,
					scrollLeftEnd   = isRtl?-scrollMaxValue:0;
                    if (isScrollMode) {
					$oScrollInner.__css({x: scrollLeftStrat})
						.__animate({x: scrollLeftEnd}, {duration: 950, delay: 1200, easing: 'easeOutCubic'});
                    }
                },
                _off = function() {
				var scrollLeftStrat = isRtl?0:-scrollMaxValue
                    if (isScrollMode) {
					$oScrollInner.__css({x: scrollLeftStrat});
				}
			},
			keydown = function(e) {
				// left
				if (e.keyCode == 37) {
					var thisX = scrollMinValue;
					var pointClass = true;
                    }
				// right
				else if (e.keyCode == 39) {
					var thisX = -scrollMaxValue;
					var pointClass = false;
				}
				$oScrollInner.__stop().__animate({x: thisX}, {duration: 450, easing: 'easeOutCubic'});
				$points.eq(0).decideClass('on', pointClass);
				$points.eq(1).decideClass('on', !pointClass);
                },
                drag = {
                    point: {
					touch: undefined,
                        thisX: 0,
					startX: 0,
					startY: 0
                    },
                    start: function(e) {
                        if (isScrollMode) {
						// e.preventDefault();
                            if (e.type == 'mousedown' && e.button > 1) {return false;}

                            isDrag = true;
                            drag.point.thisX = $oScrollInner.__stop().__css('x');
                            // drag.point.startX = e.pageX;
                            drag.point.startX = (e.type == 'touchstart')?e.originalEvent.touches[0].pageX:e.pageX;
						drag.point.touch = undefined;
						drag.point.startY = (e.type == 'touchstart')?e.originalEvent.touches[0].pageY:e.pageY;

                            if ( !isOneTagging ) {
                                isOneTagging = true;
                                GALAXY.omniture('galaxy-s10:design:whats-in-the-box:drag:screen', 'microsite_contentinter');
                            }

						if (!GALAXY.isMobile) {
							e.preventDefault();
						}

                        }
                    },
                    move: function(e) {
                            var pageX = (e.type == 'touchmove')?e.originalEvent.touches[0].pageX:e.pageX;
					var pageY = (e.type == 'touchmove')?e.originalEvent.touches[0].pageY:e.pageY;
                            var moveX = pageX-drag.point.startX;
					var moveY = pageY-drag.point.startY;

					if (isScrollMode && isDrag) {
						if (drag.point.touch === undefined ) {
							drag.point.touch = !!(Math.abs(moveX)>Math.abs(moveY));
						}
						if (drag.point.touch) {
                            $oScrollInner.__css({x: (moveX + drag.point.thisX)});
							e.preventDefault();
						}
                        }
                    },
                    end : function(e) {
                        if (isScrollMode && isDrag) {
                            var thisX = $oScrollInner.__css('x');
                            thisX = (thisX <= (-scrollMaxValue/2))? -scrollMaxValue : scrollMinValue;

						var pointClass = isRtl?!thisX:thisX;

						$points.eq(0).decideClass('on', !pointClass);
						$points.eq(1).decideClass('on', pointClass);

                            $oScrollInner.__animate({x: thisX}, {duration: 450, easing: 'easeOutCubic'});

                            isDrag = false;
                        }
                    }
                }

            return {
                on    : _on,
                off   : _off,
                resize: _resize
            }
        })();

        var S10DesignScroll = function() {
            var scrollTop = GALAXY.scrollTop;

            InfinityDisplay && InfinityDisplay.scroll(scrollTop);
            Colors && Colors.scroll(scrollTop);
        };

        var materials = (function() {
            var _init = function() {
                    if ( !$('.m_feature_materials').length ) return false;
                    hello();
                    return this;
                },
                hello = function() {
                    var $feature = $('.m_feature_materials'),
                        $areas = $feature.find('.o_areas');

                    GALAXY.hello($areas, {
                        on: function() {
                            $areas.addClass('view');
                        },
                        off: function() {
                            $areas.removeClass('view');
                        }
                    })
                }
            return {
                initalize : _init
            }
        })().initalize();

        EyeCare && GALAXY.hello('.m_feature_eye-care .f_container',{on: EyeCare.on, off: EyeCare.off});
        Colors && GALAXY.hello('.m_feature_colors .f_container',{on: Colors.on, off: Colors.off});
        Dimension && GALAXY.hello('.m_feature_dimension .f_container',{on: Dimension.on, off: Dimension.off});
        Package && GALAXY.hello('.m_feature_package .f_container', {on: Package.on, off: Package.off});

        GALAXY.hello('.change .hello-check');
        $('.o_tab').each(function(i, o) {
            var $this = $(o);
            GALAXY.hello($this, {
                on : function() {
                    $this.addClass('only_one-fade');
                }
            });
        });
        GALAXY.scrollFunctions.push(S10DesignScroll);
        // S10DesignScroll();
        GALAXY.resize(function() {
            $('html').decideClass('viewport-in', GALAXY.areaWidth <= 1920);
            Package && Package.resize(GALAXY.sizeMode);
            if ( GALAXY.sizeMode !== GALAXY.prevSizeMode) {
                OneUi && OneUi.resize();
                Colors && Colors.resize();
                EyeCare && EyeCare.resize();
                InfinityDisplay && InfinityDisplay.resize();
                Dimension && Dimension.resize();
            }
        });

        GALAXY.load(function() {
            if (GALAXY.sizeMode > 2 && Colors && location.hash == '#materials') {
                GALAXY.setScrollTop($('html, body').height());
                setTimeout(function() {
                    GALAXY.setScrollTop( $(location.hash).offset().top );
                }, 0);
            }
        });
        $window.on('hashchange', function() {
            if (GALAXY.sizeMode > 2 && Colors && location.hash == '#materials') {
                GALAXY.setScrollTop($('html, body').height());
                setTimeout(function() {
                    GALAXY.setScrollTop( $(location.hash).offset().top );
                }, 150);
            }
        });


        //<!--// 3. Javascript -->
        //


        GALAXY.initialize();
    })
})(window.jQuery);